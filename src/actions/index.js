// action creators are function that creates actions

export const selectLibrary = (libraryId) => {
    return {
        type: 'select_library', //<-- action
        payload: libraryId
    };
};