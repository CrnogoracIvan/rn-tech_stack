import { combineReducers } from 'redux';
import LibraryReducer from './LibraryReducer';
import SelectonReducer from './SelectionReducer';

export default combineReducers({
    libraries: LibraryReducer, // <-- one reducer, libraries is key and LibraryReducer is assigned to that key
    selectedLibraryId: SelectionReducer
})