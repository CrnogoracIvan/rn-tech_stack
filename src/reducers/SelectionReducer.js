export default (state = null, action) => { //<-- ako je state undefined onda ga podesi na null

    switch (action.type){
        case 'select_library':
            return action.payload;
        default:
            return state;
    }
    // return null; //u pocetku vracamo null jer ne sme da bude undefined
};