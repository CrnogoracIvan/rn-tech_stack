import React from 'react';
import { View } from 'react-native';
import { Provider } from "react-redux";
import { createStore } from 'redux';
import reducers from './reducers';
import { Header } from './components/common';
import LibraryList from './components/LibraryList';

const App = () => {
    return (
        <Provider store={createStore(reducers)}> //must be one single child component
            <View style={{flex: 1}}> //da razvuce content u celom prostoru
                <Header headerText = "Tech Stack" />
            </View>
        </Provider>
    );
};

export default App;