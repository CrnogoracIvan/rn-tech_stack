import React, { Component } from 'react';
import { Text, TouchableWithoutFeedback, View, LayoutAnimation } from 'react-native';
import { connect} from 'react-redux';
import { CardSection } from './common';
import * as actions from '../actions' //<-- zvezda znači da ubacuje sve akcije iz fajla actions/index.js i ubaci ih u var actions


class ListItem extends Component {
componentWillUnmount(){
    LayoutAnimation.spring();
}

    renderDescription() {
        const { library, expanded} = this.props

        if (expanded){
            <CardSection>
                <Text style={{flex: 1}}>
                    {library.description}
                </Text>
            </CardSection>
        
        }
    }
    
    render() {
        const {titleStyle } = styles
        const {id, title} = this.props.library;

        return(
            <TouchableWithoutFeedback
                onPress={() => this.props.selectLibrary(id)}
            >
                <View>
                    <CardSection>
                        <Text style={titleStyle}>
                            {title} //title jer imamo title u JSONu
                        </Text> 
                    </CardSection>
                    {athis.renderDescription()}
                </View>
            </TouchableWithoutFeedback>
        )
    }
}

const styles = {
    titleStyle: {
        fontSize: 18,
        paddingLeft: 15
    }
}

const mapSteteToProps = (state, ownProps) => {
    const expanded = state.selectedLibrayId === ownProps.library.id;
    return { expanded: expanded }; //ili samo {expanded}
};

export default connect(mapSteteToProps, actions) (ListItem); 